var prefMng = Components.classes["@mozilla.org/preferences-service;1"]
						.getService(Components.interfaces.nsIPrefBranch);
var flag = false;
var sAddress = "";
var sPort = "";
var oldItemCount = 0;

const NET_PRO = "network.proxy";
const NET_PRO_BACKUP = "network.proxy.backup";

function initProxer() {
						
	var startProxy = prefMng.getIntPref("network.proxy.type");
	var useProxyBox = document.getElementById("use-proxy");
	
	switch (startProxy) {
		case 0:
			useProxyBox.checked = false;
			break;
			
		case 1:
			useProxyBox.checked = true;
			break;
			
		default:
			break;
	}
	
	var proxyName = prefMng.getCharPref("network.proxy.http");
	var proxyPort = prefMng.getIntPref("network.proxy.http_port");
	
	// add event handler
	document.getElementById("proxylist").addEventListener("click", pxListener.onListClick, false);
	document.getElementById("proxylist").addEventListener("dblclick", pxListener.onDoubleClick, false);
	
	setInformationValue(proxyName, proxyPort);
	loadProxyList();
}

function setInformationValue(proxyName, proxyPort) {
	var label = proxyName + ":" + proxyPort;
	document.getElementById("proxy-name").value = label;
}

function doChange() {
	var useProxyBox = document.getElementById("use-proxy");
	
	if (useProxyBox.checked) {
		prefMng.setIntPref("network.proxy.type", 1);
	} else prefMng.setIntPref("network.proxy.type", 0);
	
	if (flag) {
		var shareProxySetting = prefMng.getBoolPref("network.proxy.share_proxy_settings");
		var oldAddress = prefMng.getCharPref("network.proxy.http");
		var oldPort = prefMng.getIntPref("network.proxy.http_port");
		
		if (!shareProxySetting) {
			prefMng.setBoolPref("network.proxy.share_proxy_settings", true);
		}
		
		setProxy(NET_PRO_BACKUP, oldAddress, oldPort);
		setProxy(NET_PRO, sAddress, sPort);
	}
	
	saveProxyList();
	window.close();
}

function addProxy() {
	var address = document.getElementById("selectedAddr");
	var port = document.getElementById("selectedPort");
	var listBox = document.getElementById("proxylist");
	
	var checkPort = parseInt(port.value);
	
	if (address.value != "" && port.value != "" && checkPort > 0 && checkPort < 65536) {
		var newListItem = document.createElement("listitem");
		var newAddrCell = document.createElement("listcell");
		var newPortCell = document.createElement("listcell");
		
		newAddrCell.setAttribute("label", address.value);
		newPortCell.setAttribute("label", port.value);
		
		newListItem.appendChild(newAddrCell);
		newListItem.appendChild(newPortCell);
		listBox.appendChild(newListItem);
		
		// scroll down the list to buttom.
		listBox.ensureIndexIsVisible(listBox.itemCount);
		
		address.value = "";
		port.value = "";
		document.getElementById("pxcmd_add").setAttribute("disabled", true);
		document.getElementById("pxcmd_replace").setAttribute("disabled", true);
	}
}

function removeProxy() {
	var listBox = document.getElementById("proxylist");
	
	if (listBox.selectedIndex != -1) {
		listBox.removeItemAt(listBox.selectedIndex);
		document.getElementById("pxcmd_remove").setAttribute("disabled", true);
	}
}

function replaceProxy() {
	var listBox = document.getElementById("proxylist");
	var newAddress = document.getElementById("selectedAddr");
	var newPort = document.getElementById("selectedPort");
	
	var checkPort = parseInt(newPort.value);
	
	if (listBox.selectedIndex != -1 && newAddress.value != "" && newPort.value != ""
		&& checkPort > 0 && checkPort < 65536) {
			
		var aItem = listBox.selectedItem;
		var address = aItem.firstChild;
		var port = aItem.lastChild;
		
		address.setAttribute("label", newAddress.value);
		port.setAttribute("label", newPort.value);
		
		newAddress.value = "";
		newPort.value = "";
		listBox.clearSelection();
		
		document.getElementById("pxcmd_replace").setAttribute("disabled", true);
		document.getElementById("pxcmd_add").setAttribute("disabled", true);
		document.getElementById("pxcmd_remove").setAttribute("disabled", true);
	}
}

function useProxy() {
	var listBox = document.getElementById("proxylist");
	if (listBox.selectedIndex != -1) {
		var aItem = listBox.selectedItem;
		sAddress = aItem.firstChild.getAttribute("label");
		sPort = aItem.lastChild.getAttribute("label");
		
		setInformationValue(sAddress, sPort);
		flag = true;
		
		document.getElementById("pxcmd_use").setAttribute("disabled", true);
		document.getElementById("pxcmd_remove").setAttribute("disabled", true);
		document.getElementById("pxcmd_replace").setAttribute("disabled", true);
		listBox.clearSelection();
	}
}

function setProxy(settingFlag, address, port) {
	port = parseInt(port, 10);

	if (port > 0 && port < 65536 && flag) {
		prefMng.setCharPref(settingFlag + ".ftp", address);
		prefMng.setCharPref(settingFlag + ".gopher", address);
		prefMng.setCharPref(settingFlag + ".socks", address);
		prefMng.setCharPref(settingFlag + ".ssl", address);
		
		prefMng.setIntPref(settingFlag + ".ftp_port", port);
		prefMng.setIntPref(settingFlag + ".gopher_port", port);
		prefMng.setIntPref(settingFlag + ".socks_port", port);
		prefMng.setIntPref(settingFlag + ".ssl_port", port);
		
		if (settingFlag == NET_PRO) {
			prefMng.setCharPref(settingFlag + ".http", address);
			prefMng.setIntPref(settingFlag + ".http_port", port);
		}
	}
}

function saveProxyList() {
	var listBox = document.getElementById("proxylist");
	var itemCount = listBox.itemCount;
	prefMng.setIntPref("extensions.proxer.listcount", itemCount);
	
	for (var i = 0; i < itemCount; i++) {
		var listItem = listBox.getItemAtIndex(i);
		var address = listItem.firstChild.getAttribute("label");
		var port = listItem.lastChild.getAttribute("label");
		
		prefMng.setCharPref("extensions.proxer.address-" + i, address);
		prefMng.setCharPref("extensions.proxer.port-" + i, port);
	}
	
	if (oldItemCount > itemCount) {
		for (var i = itemCount; i < oldItemCount; i++) {
			prefMng.clearUserPref("extensions.proxer.address-" + i);
			prefMng.clearUserPref("extensions.proxer.port-" + i);
		}
	}
}

function loadProxyList() {
	var listBox = document.getElementById("proxylist");
	oldItemCount = prefMng.getIntPref("extensions.proxer.listcount");
	
	
	for(var i = 0; i < oldItemCount; i++) {
		var newListItem = document.createElement("listitem");
		var newAddrCell = document.createElement("listcell");
		var newPortCell = document.createElement("listcell");
		
		var address = prefMng.getCharPref("extensions.proxer.address-" + i);
		var port = prefMng.getCharPref("extensions.proxer.port-" + i);
		
		newAddrCell.setAttribute("label", address);
		newPortCell.setAttribute("label", port);
		
		newListItem.appendChild(newAddrCell);
		newListItem.appendChild(newPortCell);
		listBox.appendChild(newListItem);
	}
}
