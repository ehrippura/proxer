function setCommandEnable(commandName, aFlag) {
	document.getElementById(commandName).setAttribute("disabled", !aFlag);
}

var pxListener = {
	onListClick: function() {
		var proxyList = document.getElementById("proxylist");
		var addressText = document.getElementById("selectedAddr");
		var portText = document.getElementById("selectedPort");
		
		if (proxyList.selectedIndex != -1) {
			var selected = proxyList.selectedItem;
			
			setCommandEnable("pxcmd_remove", true);
			setCommandEnable("pxcmd_use", true);
			
			addressText.value = selected.firstChild.getAttribute("label");
			portText.value = selected.lastChild.getAttribute("label");
			
			setCommandEnable("pxcmd_replace", true);
		}
	},

	onKeyType: function() {
		var addressText = document.getElementById("selectedAddr");
		var portText = document.getElementById("selectedPort");
		
		if (addressText.value == "" || portText.value == "") {
			setCommandEnable("pxcmd_add", false);
		} else {
			if (document.getElementById("proxylist").selectedIndex != -1) {
				setCommandEnable("pxcmd_replace", true);
			}
			
			setCommandEnable("pxcmd_add", true);
		}
	},
	
	onDoubleClick: function() {
		var proxyList = document.getElementById("proxylist");
		if (proxyList.selectedIndex != -1) 
			useProxy();
	}
}
